FROM openjdk:8-jdk-alpine


RUN mkdir -p /app
WORKDIR /app

COPY GCCD.java /app
COPY GCD.class /app/

#compile file java
ENTRYPOINT ["javac","GCD.java"]

#running java
ENTRYPOINT ["java","GCD"]
